<div class="mdl-card__title">
    <h2 class="mdl-card__title-text">Список постов</h2>
</div>
<div class="mdl-grid">
    <div class="mdl-layout-spacer"></div>
    <div class="mdl-cell mdl-cell--12-col">
        <ul>
            <?php foreach ($posts as $post): ?>
                <li><a href="/post/<?= $post->id ?>"><?= htmlspecialchars($post->title) ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
