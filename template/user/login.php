<div class="mdl-grid">
    <div class="mdl-layout-spacer"></div>
    <div class="mdl-cell mdl-cell--4-col">
        <div class="demo-card-wide mdl-card mdl-shadow--2dp">
            <div class="mdl-card__title">
                <h2 class="mdl-card__title-text">Авторизация</h2>
            </div>
            <div class="mdl-card__supporting-text">
                demo/demo
            </div>
            <div class="mdl-card__supporting-text">
                <form method="post">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input required class="mdl-textfield__input" type="text" name="login" id="login">
                        <label class="mdl-textfield__label" for="login">Логин</label>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input required class="mdl-textfield__input" type="text" name="password" id="password">
                        <label class="mdl-textfield__label" for="password">Пароль</label>
                    </div>
                    <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colore">
                        Войти
                    </button>
                </form>
            </div>
        </div>
    </div>
    <div class="mdl-layout-spacer"></div>
</div>