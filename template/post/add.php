<div class="mdl-layout-spacer"></div>
<div class="mdl-cell mdl-cell--12-col">
    <div class="mdl-card__title">
        <h2 class="mdl-card__title-text">Добавление поста</h2>
    </div>
    <div class="mdl-layout-spacer"></div>
    <form method="post">
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
            <input required class="mdl-textfield__input" type="text" name="title" id="title">
            <label class="mdl-textfield__label" for="title">Название</label>
        </div>
        <div class="mdl-layout-spacer"></div>
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
            <textarea required class="mdl-textfield__input" rows="5" cols="500" type="text" name="content"
                      id="content"></textarea>
            <label class="mdl-textfield__label" for="content">Текс</label>
        </div>
        <div class="mdl-layout-spacer"></div>
        <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colore">
            Добавить
        </button>
    </form>
</div>
