<div class="mdl-grid">
    <div class="mdl-layout-spacer"></div>
    <div class="mdl-cell mdl-cell--12-col">
        <div class="mdl-card__title">
            <h2 class="mdl-card__title-text"><?= htmlspecialchars($post->title) ?></h2>
        </div>
        <?= htmlspecialchars($post->content) ?>
    </div>
</div>
<div class="mdl-card__supporting-text">
    <?php foreach ($comments as $comment): ?>
        <div><?= htmlspecialchars($comment->content) ?> (c) <?= htmlspecialchars($comment->email) ?></div>
    <?php endforeach; ?>
</div>
<div class="mdl-cell mdl-cell--12-col">
    <div class="mdl-card__title">
        <h2 class="mdl-card__title-text">Добавление комментария</h2>
    </div>
    <div class="mdl-layout-spacer"></div>
    <form method="post" action="/comment/add">
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
            <input class="mdl-textfield__input" required type="email" name="email" id="email">
            <label class="mdl-textfield__label" for="email">email</label>
        </div>
        <div class="mdl-layout-spacer"></div>
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
            <textarea required class="mdl-textfield__input" rows="5" cols="500" type="text" name="content"
                      id="content"></textarea>
            <label class="mdl-textfield__label" for="content">Текс</label>
        </div>
        <div class="mdl-layout-spacer"></div>
        <input type="hidden" name="post_id" value="<?= $post->id ?>">
        <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colore">
            Добавить
        </button>
    </form>
</div>