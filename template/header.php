<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Тестовое задание</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.2.1/material.indigo-pink.min.css">
    <script defer src="https://code.getmdl.io/1.2.1/material.min.js"></script>
</head>
<body>
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
    <header class="mdl-layout__header">
        <div class="mdl-layout__header-row">
            <!-- Title -->
            <span class="mdl-layout-title">Тестовое задание</span>

            <div class="mdl-layout-spacer"></div>
            <nav class="mdl-navigation mdl-layout--large-screen-only">
                <?php if (array_key_exists('user', $_SESSION)): ?>
                    Привет&nbsp;<b><?= $_SESSION['user']['login'] ?></b>!
                <?php else: ?>
                    <a class="mdl-navigation__link" href="/user/login">Вход</a>
                <?php endif ?>
            </nav>
        </div>
    </header>
    <div class="mdl-layout__drawer">
        <span class="mdl-layout-title">Задание</span>
        <nav class="mdl-navigation">
            <a class="mdl-navigation__link" href="/blog/index">Список постов</a>
            <a class="mdl-navigation__link" href="/post/1">Открытый
                пост с комментариями</a>
            <a class="mdl-navigation__link" href="/post/add">Добавление поста</a>
            <a class="mdl-navigation__link" href="/user/login">Авторизация</a>
        </nav>
    </div
    <main class="mdl-layout__content">
        <div class="page-content">
