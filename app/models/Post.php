<?php
namespace app\models;

use padavan2030\Model;

class Post extends Model
{
    public $id;
    public $title;
    public $content;

    static public function getTable()
    {
        return 'post';
    }
}