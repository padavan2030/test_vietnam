<?php
namespace app\models;

use padavan2030\Model;

class Comment extends Model
{
    public $id;
    public $email;
    public $content;
    public $post_id;

    static public function getTable()
    {
        return 'comment';
    }
}
