<?php
namespace app\models;

use padavan2030\Model;

class User extends Model
{
    public $id;
    public $login;
    public $password;

    static public function getTable()
    {
        return 'user';
    }
}