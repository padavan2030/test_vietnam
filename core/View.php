<?php
namespace padavan2030;

class View
{
    /**
     * @param string $path
     * @param array $data
     * @return string
     * @throws \Exception
     */
    public static function render($path, Array $data = [])
    {
        ob_start();
        self::includeFile('header.php');
        self::includeFile($path, $data);
        self::includeFile('footer.php');
        $content = ob_get_contents();
        ob_end_clean();

        return $content;
    }

    /**
     * @param string $path
     * @throws \Exception
     */
    protected static function includeFile($path, $data = [])
    {
        extract($data);
        unset($data);
        $fullPath = '../template/'.$path;
        if (file_exists($fullPath)) {
            include($fullPath);
        } else {
            throw new \Exception("File '$fullPath' not exist.");
        }
    }
}
