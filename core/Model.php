<?php
namespace padavan2030;

use padavan2030\Db;

interface iModel
{
    /**
     * @return string
     */
    static public function getTable();
}

abstract class Model implements iModel
{
    /**
     * @param array $values
     */
    public function setAttributes(Array $values)
    {
        if (is_array($values)) {
            foreach ($values as $name => $value) {
                $this->setAttribute($name, $value);
            }
        }
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function setAttribute($name, $value)
    {
        if (property_exists($this, $name)) {
            $this->$name = $value;
        }
    }

    /**
     * @param null $condition
     * @param array $params
     * @return null|static
     */
    static public function findOneByAttributes($condition = null, $params = [])
    {
        $conn = Db::getInstance()->getConnection();
        $where = '';
        if ($condition) {
            $where = 'WHERE '.$condition;
        }
        $sth = $conn->prepare('SELECT * FROM '.static::getTable().' '.$where.' LIMIT 1');
        $sth->execute($params);
        $data = $sth->fetch(\PDO::FETCH_ASSOC);
        if ($data) {
            $model = new static();
            $model->setAttributes($data);

            return $model;
        }

        return null;
    }

    /**
     * @param null $condition
     * @param array $params
     * @return static[]|null
     */
    static public function findAllByAttributes($condition = null, $params = [])
    {
        $conn = Db::getInstance()->getConnection();
        $where = '';
        if ($condition) {
            $where = 'WHERE '.$condition;
        }
        $sth = $conn->prepare('SELECT * FROM '.static::getTable().' '.$where);
        $sth->execute($params);
        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);
        if ($data) {
            $models = [];
            foreach ($data as $item) {
                $model = new static();
                $model->setAttributes($item);
                $models [] = $model;
            }

            return $models;
        }

        return null;
    }

    public function save()
    {
        $conn = Db::getInstance()->getConnection();
        $data = $conn->query('PRAGMA table_info('.static::getTable().');')->fetchAll(\PDO::FETCH_ASSOC);
        $pk = $data[array_search(1, array_column($data, 'pk'))]['name'];
        $attributes = array_column($data, 'name');
        $properties = [];
        foreach ($attributes as $attribute) {
            if (property_exists($this, $attribute) && $this->{$attribute} !== null) {
                $properties[$attribute] = $this->{$attribute};
            }
        }
        if (count($properties)) {
            $values = $prepare = [];
            if (array_key_exists($pk, $properties)) {
                $pkValuse = $properties[$pk];
                unset($properties[$pk]);
                foreach ($properties as $property => $value) {
                    $prepare [] = $property.'=?';
                    $values [] = $value;
                }
                $values [] = $pkValuse;
                $query = $conn->prepare(
                    'UPDATE  '.static::getTable().' SET '.implode(',', $prepare).' WHERE '.$pk.' = ?'
                );
                $query->execute($values);
            } else {
                foreach ($properties as $property => $value) {
                    $values [] = $value;
                    $prepare [] = '?';
                }
                $query = $conn->prepare(
                    'INSERT INTO  '.static::getTable().' ('.implode(',', array_keys($properties)).') VALUES ('.implode(
                        ',',
                        array_fill(0, count($properties), '?')
                    ).')'
                );
                $query->execute($values);
                $this->{$pk} = $conn->lastInsertId();
            }
        }

    }
}