<?php
namespace padavan2030;

class Router
{
    protected $routes = [];
    protected $vars;

    /**
     * @param string $method
     * @param string $pattern
     * @param callable $callback
     */
    public function addRoute($method, $pattern, callable $callback)
    {
        $this->routes[] = [$method, $pattern, $callback];
    }

    public function run()
    {
        foreach ($this->getRoutes() as $route) {
            list($method, $pattern, $callback) = $route;
            if ($method !== $_SERVER['REQUEST_METHOD'] || !preg_match(
                    '~^'.$pattern.'$~i',
                    trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/'),
                    $matches
                )
            ) {
                continue;
            }
            $keys = array_filter(
                array_keys($matches),
                function ($k) {
                    return !is_int($k);
                }
            );
            $params = array_intersect_key($matches, array_flip($keys));
            call_user_func($callback, $params);

            return;
        }
        http_response_code(404);

        return;
    }

    /**
     * @return array
     */
    public function getRoutes()
    {
        return $this->routes;
    }
}