<?php
namespace padavan2030;

class Response
{
    protected $content;
    protected $status_code = 200;
    protected $headers = [];

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return Response
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->status_code;
    }

    /**
     * @param int $status_code
     * @return Response
     */
    public function setStatusCode($status_code)
    {
        $this->status_code = $status_code;

        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param $name
     * @param $value
     */
    public function addHeader($name, $value)
    {
        $this->headers[$name] = $value;
    }

    public function sendHeaders()
    {
        http_response_code($this->getStatusCode());
        foreach ($this->getHeaders() as $name => $value) {
            header("$name: $value");
        }
    }

    public function sendContent()
    {
        echo $this->getContent();
    }

    public function send()
    {
        $this->sendHeaders();
        $this->sendContent();
    }

    public function redirect($url)
    {
        $this->setStatusCode(302);
        $this->addHeader('Location', $url);
        $this->sendHeaders();
    }
}