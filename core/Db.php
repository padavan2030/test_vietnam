<?php
namespace padavan2030;

class Db
{
    private static $_instance;
    private $_connection;

    private function __construct()
    {
        try {
            $this->_connection = new \PDO('sqlite:../data/database.sqlite');
        } catch (\Exception $exception) {
            echo $exception->getMessage();
        }
    }

    /**
     * @return static
     */
    public static function getInstance()
    {
        if (self::$_instance === null) {
            $class = get_called_class();
            self::$_instance = new $class();
        }

        return self::$_instance;
    }

    public function getConnection()
    {
        return $this->_connection;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }
}