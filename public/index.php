<?php
use app\models\Comment;
use app\models\Post;
use app\models\User;
use padavan2030\Response;
use padavan2030\Router;
use padavan2030\View;

require __DIR__.'/../vendor/autoload.php';
$config = require(__DIR__.'/../config/web.php');
session_start();

$router = new Router();

$router->addRoute(
    'GET',
    '',
    function () {
        (new Response())->setContent(View::render('index.php'))->send();
    }
);

$router->addRoute(
    'GET',
    'user/login',
    function () {
        if (array_key_exists('user', $_SESSION)) {
            (new Response())->redirect('/');
        } else {
            (new Response())->setContent(View::render('user/login.php'))->send();
        }
    }
);
$router->addRoute(
    'POST',
    'user/login',
    function () {
        $user = User::findOneByAttributes(
            'login = :login AND password = :password',
            [
                'login' => $_POST['login'],
                'password' => $_POST['password'],
            ]
        );
        if ($user) {
            $_SESSION['user'] = get_object_vars($user);
            (new Response())->redirect('/');
        } else {
            (new Response())->redirect('/user/login');
        }
    }
);

$router->addRoute(
    'GET',
    'blog/index',
    function () {
        $posts = Post::findAllByAttributes();
        (new Response())->setContent(View::render('blog/index.php', ['posts' => $posts]))->send();
    }
);
$router->addRoute(
    'GET',
    'post/(?<id>\d+)',
    function ($args) {
        $post = Post::findOneByAttributes('id = :id', ['id' => $args['id']]);
        if ($post) {
            $comments = Comment::findAllByAttributes('post_id = :post_id', ['post_id' => $args['id']]);
            (new Response())->setContent(
                View::render('post/view.php', ['post' => $post, 'comments' => $comments])
            )->send();
        } else {
            (new Response())->setStatusCode(404)->setContent('Пост не найден')->send();

        }
    }
);
$router->addRoute(
    'GET',
    'post/add',
    function () {
        if (array_key_exists('user', $_SESSION)) {
            (new Response())->setContent(View::render('post/add.php'))->send();
        } else {
            (new Response())->setStatusCode(403)->setContent('Доступ запрещен')->send();
        }
    }
);
$router->addRoute(
    'POST',
    'post/add',
    function () {
        if (array_key_exists('user', $_SESSION)) {
            $post = new Post();
            $post->setAttributes($_POST);
            $post->save();
            (new Response())->redirect('/post/'.$post->id);
        } else {
            (new Response())->setStatusCode(403)->setContent('Доступ запрещен')->send();
        }
    }
);
$router->addRoute(
    'POST',
    'comment/add',
    function () {
        $comment = new Comment();
        $comment->setAttributes($_POST);
        $comment->save();
        (new Response())->redirect('/post/'.$comment->post_id);
    }
);

$router->run();